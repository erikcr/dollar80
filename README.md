# dollar80
Simple counter based on GaryVee's $1.80 strategy.

## About
I have been experimenting with the Quasar framework for quite some time, but I had yet to work with the Cordova wrapper. Then I heard Gary Vaynerchuk's $1.80 [strategy](https://www.garyvaynerchuk.com/1-80-instagram-strategy-grow-business-brand/) for follower growth and figured that I could build something for that.

What I ended up with was a stupid simple counter app that increments by two instead of one :astonished:.

### The App
You can download the app from the Google Play Store [here](https://play.google.com/store/apps/details?id=io.cordova.dollar80&hl=en).

## Build Setup

``` bash
# install dependencies
$ yarn install # or npm install

# serve with hot reload at localhost:8080
$ quasar dev

# build for production with minification
$ quasar build

# lint code
$ quasar lint
```

### Android build
This assumes you already have Cordova and Android Studio installed and configured. View the Quasar [docs](http://quasar-framework.org/guide/cordova-wrapper.html) for getting those setup as well as tips and troubleshooting.

``` bash
$ cd cordova

$ cordova platform add android

# ensure system is properly configured
$ cordova requirements

# if that returns successful, build the APK file
$ cordova build
```
