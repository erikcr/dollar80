import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const TOKEN = 'cents'

export default new Vuex.Store({
  state: {
    facebook: 0.00,
    instagram: 0.00,
    twitter: 0.00
  },

  getters: {
    total: (state) => {
      let total = 0
      for (let platform in state) {
        total += state[platform]
      }
      return total
    }
  },

  mutations: {
    INIT (state) {
      for (let platform in state) {
        state[platform] = JSON.parse(localStorage.getItem(`${TOKEN}_${platform}`))
      }
    },

    ADD_2_CENTS (state, platform) {
      state[platform] += 0.02
    },

    RESET (state, platform) {
      state[platform] = 0.00
    }
  },

  actions: {
    add2Cents ({ commit, state }, platform) {
      commit('ADD_2_CENTS', platform)
      localStorage.setItem(
        `${TOKEN}_${platform}`,
        JSON.stringify(state[platform])
      )
    },

    reset ({ commit, state }) {
      for (let platform in state) {
        commit('RESET', platform)
        localStorage.setItem(
          `${TOKEN}_${platform}`,
          JSON.stringify(state[platform])
        )
      }
    }
  }
})
